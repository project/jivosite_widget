JIVOSITE WIDGET
----------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation and configuration
 * Credits / Contact


INTRODUCTION
------------

JivoSite livechat is simple and elegant communication solution for your
e-commerce site. It resembles Facebook chat window that is quite familiar
nowadays. Also chat window always stays on top of your web site and does
not blink and reload upon navigation. It will not get buried under dozens
of opened browser windows and provide smooth user experience. If nobody is
online visitor will see offline email form.


INSTALLATION AND CONFIGURATION
------------------------------

1 - Download and enable this module
2 - Go to admin/config/services/jivosite_widget and fill out all required fields.
3 - After you press "Save" button, a new Jivosite account and widget will be
created for you.
4 - Go to admin/config/services/jivosite_widget/login and click "Login" button to
login your Jivosite account. There you can configure various settings
of your live chat.
The Jivosite module integrates Jivosite live support widget
into your Drupal site.
More information at: Installing contributed modules (Drupal 7) [18].


CREDITS / CONTACT
-----------------

This module was sponsored by:
Alex_optim - email: o.bazyliuk@gmail.com
