<?php

/**
 * @file
 * Admin page callbacks for the jivosite_widget module.
 */

/**
 * Form bulder. Returns install widget form.
 */
function jivosite_widget_settings_install_form($form, &$form_state) {
  // We want to deal with nested form array.
  $form['#tree'] = TRUE;

  $form['logo'] = array(
    '#markup' => '<div>' . _jivosite_widget_logo() . '</div>',
  );

  // Get widget ID.
  $widget = _jivosite_widget_settings('install', 'widgetID');

  if (!$widget) {
    $text = t("Let\'s install a new JivoSite widget. If you have already a JivoSite account, we\'ll try to connect to the existing account.");

    $form['text'] = array(
      '#markup' => $text,
    );
  }

  // Output fields.
  $form['install']['email'] = array(
    '#title' => t('E-mail'),
    '#required' => TRUE,
    '#type' => 'textfield',
    '#default_value' => _jivosite_widget_settings('install', 'email'),
    '#description' => t('Specify email to be used to login into JivoSite admin panel, desktop application and for JivoSite notifications.'),
  );

  $form['install']['userPassword'] = array(
    '#title' => t('Password'),
    '#required' => TRUE,
    '#type' => 'textfield',
    '#default_value' => _jivosite_widget_settings('install', 'userPassword'),
    '#description' => t('Specify password for JivoSite. If you already have JivoSite account you can use the existing password'),
  );

  $form['install']['userDisplayName'] = array(
    '#title' => t('Your name'),
    '#required' => TRUE,
    '#type' => 'textfield',
    '#default_value' => _jivosite_widget_settings('install', 'userDisplayName'),
    '#description' => t('Specify name that will be shown to site visitors.'),
  );

  // Pass site URL via hidden field.
  global $base_url;
  $form['install']['siteUrl'] = array(
    '#type' => 'value',
    '#value' => $base_url,
  );

  // Pass token via hidden field.
  $form['install']['authToken'] = array(
    '#type' => 'value',
    '#value' => user_password(14),
  );

  $form['install']['partnerId'] = array(
    '#type' => 'value',
    '#value' => 'drupal_module',
  );

  $form['install']['partnerPassword'] = array(
    '#type' => 'value',
    '#value' => ' ',
  );

  $form['install']['widgetID'] = array(
    '#type' => 'value',
    '#value' => '',
  );

  $form['setup'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form bulder. Returns module settings form.
 */
function jivosite_widget_settings_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['settings']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show on specific pages'),
    '#options' => array(
      t('All pages except those listed'),
      t('Only the listed pages'),
    ),
    '#default_value' => _jivosite_widget_settings('settings', 'visibility'),
  );
  $form['settings']['paths'] = array(
    '#title' => t('Paths'),
    '#type' => 'textarea',
    '#default_value' => _jivosite_widget_settings('settings', 'paths'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %user for the current user's page and %user-wildcard for every user page. %front is the front page.", array(
      '%user' => 'user',
      '%user-wildcard' => 'user/*',
      '%front' => '<front>',
    )),
  );
  $form['settings']['buttons'] = array(
    '#title' => t('Buttons'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#parents' => array('settings'),
    '#description' => t('Configure buttons in your <a href="@admin">JivoSite account</a> and then copy and paste the button codes into the text fields below. A <a href="@url">block</a> will be created for each button so you can put it everywhere on the site', array(
      '@url' => url('admin/structure/block'),
      '@admin' => 'https://admin.jivosite.com/buttons',
    )),
  );
  $form['settings']['buttons']['chat'] = array(
    '#title' => t('Chat button'),
    '#type' => 'textarea',
    '#default_value' => _jivosite_widget_settings('settings', 'chat'),
    '#description' => t('CSS and HTML code for chat button.'),
  );
  $form['settings']['buttons']['call'] = array(
    '#title' => t('Call button'),
    '#type' => 'textarea',
    '#default_value' => _jivosite_widget_settings('settings', 'call'),
    '#description' => t('CSS and HTML code for call button.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form bulder. Returns JivoSite's login form.
 */
function jivosite_widget_settings_login_form($form, &$form_state) {

  $form['#attributes'] = array('target' => '_blank');
  $form['#action'] = url(JIVOSITE_WIDGET_LOGIN_URL, array('external' => TRUE));

  $form['token'] = array(
    '#type' => 'hidden',
    '#value' => _jivosite_widget_settings('install', 'authToken'),
  );

  $form['partnerId'] = array(
    '#type' => 'hidden',
    '#value' => 'drupal_module',
  );

  $form['login'] = array(
    '#type' => 'submit',
    '#value' => t('Login to JivoSite Control Panel'),
  );

  return $form;
}

/**
 * Form bulder. Returns JivoSite's desktop app.
 */
function jivosite_widget_settings_app_form($form, &$form_state) {
  $form['logo'] = array(
    '#markup' => _jivosite_widget_logo(),
  );

  $form['app'] = array(
    '#markup' => theme('jivosite_widget_desktop_app'),
  );

  return $form;
}

/**
 * Validate function. Performs POST query and validates JivoSite's response.
 */
function jivosite_widget_settings_install_form_validate($form, &$form_state) {
  // Cleanup submitted values.
  form_state_values_clean($form_state);

  $email = trim($form_state['values']['install']['email']);
  $username = trim($form_state['values']['install']['userDisplayName']);
  $password = trim($form_state['values']['install']['userPassword']);

  if ($email && $username && $password) {
    // Build string for POST query from PHP array.
    $data = drupal_http_build_query($form_state['values']['install']);
    $options = array(
      'method' => 'POST',
      'data' => $data,
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    // Perform POST query.
    $response = drupal_http_request(JIVOSITE_WIDGET_INSTALL_URL, $options);

    // Now let's check the response.
    // 200 means OK, if not - something gone wrong.
    if ($response->code != 200) {
      $codes = jivosite_widget_http_codes();
      if (isset($codes[$response->code])) {
        $error = $codes[$response->code];
      }
      else {
        // Unknown error.
        $error = t('A weird error has occurred');
      }
    }

    // No data received at all. Log it and exit.
    if (empty($response->data)) {
      form_set_error('', t('No data received'));

      return;
    }

    // Check if response contains "Error:".
    if (strpos($response->data, 'Error:') !== FALSE) {
      $error = str_replace('Error:', '', $response->data);
    }

    // Display errors and exit.
    if (!empty($error)) {
      form_set_error('', t('Oops! JivoSite server answered: @error', array(
        '@error' => $error,
      )));

      return;
    }

    // Set received widget id for further processing.
    form_set_value($form['install']['widgetID'], $response->data, $form_state);
  }
}

/**
 * Submit function. Save install form values in database.
 */
function jivosite_widget_settings_install_form_submit($form, &$form_state) {
  $widget = _jivosite_widget_settings('install', 'widgetID');
  $settings = variable_get('jivosite_widget_settings', array());
  $settings['install'] = $form_state['values']['install'];
  variable_set('jivosite_widget_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
  if (!$widget) {
    $form_state['redirect'] = 'admin/config/services/jivosite_widget/desktop';
  }
}

/**
 * Submit function. Save settings form values in database.
 */
function jivosite_widget_settings_form_submit($form, &$form_state) {
  $settings = variable_get('jivosite_widget_settings', array());
  $settings['settings'] = $form_state['values']['settings'];
  variable_set('jivosite_widget_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
}
